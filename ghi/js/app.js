function createCard(name, description, starts, ends, pictureUrl, location) {
    return `
    <div class="card shadow mb-5 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class = 'card-footer'> <time>${starts} - ${ends}</time> </div>
    </div>
  `;

}


function alert() {
    return `
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Well done!</h4>
        <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
        <hr>
        <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col')
    let colIndx = 0
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        return alert();
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const starts = new Date(details.conference.starts).toLocaleDateString();
                const ends = new Date(details.conference.ends);
                const pictureUrl = details.conference.location.picture_url;
                const location = details.conference.location.name;
                const html = createCard(name, description, starts, ends, pictureUrl, location);
                const column = columns[colIndx % 3];
                column.innerHTML += html;
                colIndx = (colIndx + 1) % 3;
        }
        
        
        }
  
      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }
  
  });
